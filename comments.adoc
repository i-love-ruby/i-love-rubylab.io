=== Comments

Comments are small pieces of notes you can put into a program so that you or someone else when going through the program 7,658 years from now will remember or come to know what its doing. You may be smart today, but tomorrow you may not be as smart as you are now, your boss or client who has paid you will yell upon you at that moment to fix a priority bug or to update a software. Open your text editor and type this code:

[source, ruby]
----
puts "Hello I am Zigor, a automated Robot that says Hello"
print "Please enter your name:"
name = gets()
puts "Hello #{name}"
----

You might be able to understand it now, but after 7,658 years footnote:[You can live so long if science progresses fast enough. Researches have data to make you live so long! So be hopeful.]? At that time you might have forgotten Ruby altogether! So start commenting. See the same program `comment.rb` below, how it looks like?

[source, ruby]
----
include::code/comment.rb[]
----

Look at the code above, you have told something about client in the first three lines. These lines start with a # (hash or check sign). The thing that follows after a check sign is a comment, comments don't interfere with programs execution, but it can be used to provide visual hints to humans of what's going on in the program.

Now lets look at this line

[source, ruby]
----
puts "Hello #{name}" # Embeds name into the string that gets printed
----

Here you have `#{name}` enclosed within double quotes, hence it's treated as an embedded ruby code in a string rather than a comment, whereas # Embeds name into the string that gets printed is treated as comment.

So I hope you understand that comment can one day help. Professionals always comment when they write code. They will take pains so that almost any Ruby coder who reads their program will be able to understand how it works. 

==== Multiline Comments

If you want to put a lot of comment about the size of a paragraph, then you can put that piece of text between `=begin` and `=end` as shown in the program `comments_multiline.rb` below

[source, ruby]
----
include::code/comments_multiline.rb[]
----

In the code above note how we put these text:

[source, ruby]
----
 The client is an idiot
 he wants me to update a software after 7,658 years.
 The hell with him
----

Between `=begin` and `=end`, when you execute the program, those between the `=begin` and `=end` will be ignored. So don't hesitate to write a lot of comment, as now you know there is a way to do it, and it will benefit you and your fellow programmers greatly.

There is one small thing you must know about `=begin` and `=end`, that is they must start from the first column, there should not be any spaces before the `=` sign, if there is, ruby will think there it's a programming mistake and will signal an error.

