=== elsif

When we use `if` and `else`, the code under `if` gets executed if the condition is satisfied, else the code under `else` section gets executed. Let's have a new scenario where the code under `if` is not satisfied, then the program immediately jumps to the `else` section, now the logic demands that we need to check another condition at the `else` level too, what should we do? To deal with such a scenario we can use the `elsif` command. Take a look at the code below

[source, ruby]
----
include::code/elsif.rb[]
----

When executed it produces the following result

----
b = 7 is greatest
----

Lets walkthru the code step by step. Let's look at the line

[source, ruby]
----
a,b,c = 3,7,5
----

In this line we assign values 3, 7 and 5 to variables `a,b` and `c`. Let's now come to the if statement

[source, ruby]
----
if a > b and a > c
----

In this statement we check if `a` is greater than `b` and if `a` is greater than `c`. Note the keyword `and`. The `if` condition is satisfied only if both conditions are true. `a` is less than `b` hence this condition fails so program skips the `if` statement and comes to the `elsif` statement

[source, ruby]
----
elsif b > c and b > a 
----

`elsif` is `else` plus `if`, here we check on another two conditions that's separated by `and`, we check if `b `is greater than `a` and if `b` is greater than `c`, both are true and hence the statement under `elsif`

[source, ruby]
----
puts "b = #{b} is greatest"
----

gets executed and we get the result. Since the `elsif` is satisfied other `else` and the code that comes under it is ignored.

