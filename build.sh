asciidoctor book.adoc
asciidoctor-epub3 book.adoc -o ilr.epub
mv ilr.epub ../ilr-output/
asciidoctor-pdf book.adoc -o ilr.pdf
mv ilr.pdf ../ilr-output/
