=== case when and regular expressions

Case statements can match regular expressions too. Read Regular Expressions section to understand the example below.

[source, ruby]
----
include::code/case_when_regexp.rb[]
----

Output

----
string contains Ruby
----

In the example above check the statement `when /Ruby/`, it checks whether the expression `/Ruby/` appears in `string`. In above example it does appear. So it prints out `string contains Ruby`.

