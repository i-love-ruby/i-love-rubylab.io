== Safe Navigation

Safe navigation is a way to write conditions that don't throw unexpected errors. See the program below, type it and execute it:

.{code_url}/safe_navigation.rb[safe_navigation.rb]
[source, ruby]
----
include::code/safe_navigation.rb[]
----

Output

----
The robots name is Zigor
----

So the program executes perfectly without a glitch. Look at the code `if robot&.name`, we will understand the significance of it shortly.

Now let's take a situation where the robot is not initialized. That is robot = Robot.new is not written, the program looks something as shown below

.{code_url}/safe_navigation_2.rb[safe_navigation_2.rb]
[source, ruby]
----
include::code/safe_navigation_2.rb[]
----

When we execute the program above it still does not throw an error!! Now look at the code `if robot&.name`, it does the trick, we will see how. Type the program below and execute it:

.{code_url}/not_safe_navigation.rb[not_safe_navigation.rb]
[source, ruby]
----
include::code/not_safe_navigation.rb[]
----

Output

----
not_safe_navigation.rb:4:in `<main>': undefined method `name' for nil:NilClass (NoMethodError)
----

So, this one throws an error. But here we have written the `if` condition as `if robot.name`, and we haven't used the safe navigation. Now we know the scenario. First we must check if the variable `robot` exists, or it's not `nil`, and if the `robot.name` too is not `nil`, then we must not print the thing. So to correct {code_url}/not_safe_navigation.rb[not_safe_navigation.rb] we type in the following code.

.{code_url}/not_safe_navigation_2.rb[not_safe_navigation_2.rb]
[source, ruby]
----
include::code/not_safe_navigation_2.rb[]
----

Look how we need to provide long condition with an and operator here as shown:

[source, ruby]
----
if robot and robot.name
----

instead we can simply write it as:

[source, ruby]
----
if robot&.name
----

as shown in {code_url}/safe_navigation.rb[safe_navigation.rb] which is convenient.

