=== Getting Input

A program is more useful when it interacts with the user, let's write a program that asks us our name and says hello to us. Type the following code (I saved it as say_hello.rb)

[source, ruby]
----
include::code/say_hello.rb[]
----

Now run it, this is how the output will look like

[source]
----
Hello I am Zigor, a automated Robot that says Hello 
Please enter your name:Karthik 
Hello Karthik 
----

Lets walkthru the program

The first line

[source, ruby]
----
puts "Hello I am Zigor, a automated Robot that says Hello"
----


Prints that the program name is Zigor and it's a automated robot that wishes you Hello. Then it prints a line feed, hence the content that's printed then on goes to the next line

The second line

[source, ruby]
----
print "Please enter your name:"
----

prints out `"Please enter your name:"`, note that we have used `print` here, not `puts` because we want to get the user's name right after `name:`, I feel it will be awkward if we let them type name in the next line, so to avoid the line feed I am using `print` instead of `puts`.

When the user enters name and presses enter, it is caught by the `gets()` function and the thing you typed is stored in the variable called name because of this piece of code

[source, ruby]
----
name = gets()
----

Now all our Zigor needs to do is to wish hello, for which we use this code

[source, ruby]
----
puts "Hello #{name}"
----

Notice how we are embedding the variable name into string by putting it between `#{` and `}`. The same effect can be achieved by using code like this

[source, ruby]
----
puts "Hello "+name
----

But doesn't the former piece of code look better? It's all your choice. Ruby lets you do the same thing in many ways. You can choose anything that you feel comfortable.

Any way in this topic the line you must be looking at is the one that has `gets()` method or function, it waits for a keyboard input, when you give an input and press enter, it takes your input and assigns the value to variable, in this case the variable is name.

