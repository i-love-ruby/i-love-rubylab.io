=== Variables

Variables are something that stores value in it. You can imagine them as a box which can hold pebbles. If a box named `a` holds five pebbles then its value is 5, if another box `b` holds three pebbles, then its value is 3. Let say you got a new box `c` and you want its value to be the sum of box `a` and box `b`, then you simply add number of pebbles in `a` and `b`, it totals to 8, you put 8 pebbles in `c` to make `c = a + b`. I hope you have got a hint what a variable is. Let's program it in Ruby

[source, ruby]
----
>> a = 5 
=> 5 
>> b = 3 
=> 3 
>> c = a + b 
=> 8
----

Let's try another problem, I buy 50 mangoes from a farmer at &#8377; 10/- and bring it to the market and sell it at &#8377; 15/- each, what is my profit?

*Answer:*

OK first I have 50 mangoes so in irb I type as shown:

[source, ruby]
----
>> mangoes = 50 
=> 50
----

So I have assigned the value of 50 to a variable called `mangoes`. Next I declare and assign a value of 10 to a variable `buy_price` as shown:

[source, ruby]
----
>> buy_price = 10 
=> 10
----

Similarly, I assign 15 to a variable named `sell_price`

[source, ruby]
----
>> sell_price = 15 
=> 15
----
 
Now profit per mango is the difference between sell and buy price, hence I can calculate it as shown

[source, ruby]
----
>> profit = sell_price - buy_price 
=> 5
----

By selling a mango I get a profit of Rs 5/-, what will I get by selling 50 mangoes? It's a multiple of `profit` with `mangoes` and we get it as shown

[source, ruby]
----
>> total_profit = profit * mangoes 
=> 250
----

So by selling 50 mangoes we can earn a profit of &#8377; 250/-. Lets say that we have bought 72 mangoes, now we want to know what profit would be, this can be easily done by changing or varying the value of `mangoes` from 50 to 72 and recalculating the `total_profit` as shown below

[source, ruby]
----
>> mangoes = 72
>> total_profit = profit * mangoes 
=> 360
----

Now you may know why we call these things are variables, a variable is a box that can contain any value it wants. Just like you can add or take away pebbles from a box, you can do the same to math operation to variables.

==== Naming Convention

In the mango example, you would have noticed that I have given the names of variables as `buy_price`, `sell_price`, `total_profit` and not as buy `price`, `sell price`, `total profit`, why so? It turns out that one must follow a certain naming convention or rules when naming a variable. The rules of naming a variable are as follows:

* There must be no space in between variable names
* There must be no special character except underscore ` _` in a variable name
* A variable name can have numbers
** A variable name must not start with a number
* A variable must either start with a character or an underscore
** Capital character should not appear at the start of variable
        
Below given are examples of valid variable names

[source, ruby]
----
mango
total_price
mango_
_mango
buyPrice
boeing747
boeing_747
iam23yrsold
----

Below are given examples of invalid variable names

[source, ruby]
----
34signals
Mango
total cost
----

==== The underscore - a special variable

Suppose we want to find whats 87 raised to the power 12, we can do as follows

[source, ruby]
----
>> 87 ** 12 
=> 188031682201497672618081
----

Now we want to multiply the result with 5 and see the answer, now the above result is a whoppy 24footnote:[To find the number of digits type his in irb: (87**12).to_s.length] digit number and we must type all of it and put a star five to get an answer, that's a lot of work! If you are a programmer, laziness should flow in your veins otherwise find another profession. One way is to assign this value to a variable and multiply it by 5 as shown below

[source, ruby]
----
>> a = 87 ** 12 
=> 188031682201497672618081 
>> a*5 
=> 940158411007488363090405
----

However there is another easy way as shown below

[source, ruby]
----
>> 87 ** 12 
=> 188031682201497672618081 
>> _ * 5 
=> 940158411007488363090405
----

I did find out 87 raised to the power of 12, and after that I multiplied underscore `_` with five! But how come? Underscore is a special kind of variable, in it the result of last execution gets stored automatically. If you want to use the last obtained output you can do so by using underscore `_` as a variable footnote:[This underscore as a variable works only in interactive ruby (irb). When you are executing a ruby program typed in a file this wont work. See section Underscore in Appendix].

