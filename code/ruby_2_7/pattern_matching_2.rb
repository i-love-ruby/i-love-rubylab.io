# pattern_matching_2.rb

case [0, [1, 2, 3]]
in [0, [a, 2, b]]
  p a #=> 1
  p b #=> 3
end
