names = ["gandhi", "buddha", "india"]

# bad
p names.map { |name| name.upcase }

# good
p names.map(&:upcase)

# also good?
p names.map { _1.upcase }
