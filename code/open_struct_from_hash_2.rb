# open_struct_from_hash_2.rb

require 'ostruct'

hash = {
  "name" => "Karthik",
  "age" => 39,
  "profession" => "Engineer"
}

p = OpenStruct.new hash
puts "Hello, I am #{p.name}, age #{p.age}, and I am an #{p.profession}"
