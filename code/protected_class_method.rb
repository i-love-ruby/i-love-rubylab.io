# protected_class_method.rb

class Someclass
  protected
  def self.some_protected_method
    puts "In some_protected_method"
  end
end

Someclass.some_protected_method
