# private_class_method.rb

class Someclass
  private
  def self.some_private_method
    puts "In some_private_method"
  end
end

Someclass.some_private_method
