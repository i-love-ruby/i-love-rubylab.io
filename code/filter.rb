# filter.rb

def greater_than_7 num
  num > 7
end

p (1..10).filter { |x| greater_than_7 x }
