# without_flip_flop.rb

print_it = nil

1.upto 10 do |i|
  print_it = true if i >= 5
  print_it = false if i > 8

  if print_it
    puts i
  end
end
