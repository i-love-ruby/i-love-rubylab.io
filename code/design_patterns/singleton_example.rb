# singleton_example.rb

require 'singleton'

class SingletonExample
  include Singleton
end

puts SingletonExample.instance.object_id
puts SingletonExample.instance.object_id
