# singleton_example.rb

require 'singleton'

class SingletonExample
  include Singleton

  def self.a_method
  end
end

puts SingletonExample.a_method.object_id
puts SingletonExample.a_method.object_id

