Gem::Specification.new do |s|
  s.name        = 'mindaslab_hello'
  s.version     = '0.0.0'
  s.date        = '2018-12-02'
  s.summary     = "A gem from Karthikeyan A K"
  s.authors     = ["Karthikeyan A K"]
  s.email       = 'mindaslab@protonmail.com'
  s.files       = ["lib/mindaslab_hello.rb"]
end