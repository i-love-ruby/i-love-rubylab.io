# argument_forwarding_3.rb

def rest_of_arguments *args
  puts "In rest_of_arguments"
  puts args
end

def first_argument_and_forward_others(a, ...)
  puts "In first_argument_and_forward_others"
  puts a
  rest_of_arguments(...)
end

first_argument_and_forward_others(1, 2, 3, 4, 5)
