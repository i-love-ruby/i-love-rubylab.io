# double_pipe.rb

name = "Zigor"

if name == "Zigor" || name == "R2D2"
  puts "#{name} is a Robot"
else
  puts "#{name} may not be a robot"
end
