# private_protected_method_in_inheritance.rb

class A

  private

  def private_method
    puts "In A#private_method"
  end

  def self.private_class_method
    puts "In A#private_class_method"
  end

  protected

  def protected_method
    puts "In A#protected_method"
  end

  def self.protected_class_method
    puts "In A#protected_class_method"
  end

end

class B < A

  def call_private_method
    private_method
  end

  def call_protected_method
    protected_method
  end
end

class C
  def call_private_class_method
    A.private_class_method
  end

  def call_protected_class_method
    A.protected_class_method
  end
end

B.new.call_private_method
B.new.call_protected_method
C.new.call_protected_class_method
C.new.call_private_class_method
