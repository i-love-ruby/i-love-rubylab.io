=== Jupyter Lab

Ruby's REPL that you can invoke by typing `irb` in your terminal is good, but imagine if you can run  a vey interactive REPL in your browser. A tool called Jupyter does just that. One can learn about Jupyter here https://jupyter.org/ , one may install it on your computer. For that you need to have Python installed first.

Once you have done it you can install a gem called iruby by typing

```
$ gem install iruby
```

in your terminal.

Once done you can launch Jupyter lab by typing:

```
$ jupyter lab
```

Your browser will open up and you will get a screen as shown:

image::jupyter_lab-2b713.png[]

In the above picture one can see that I have integrated Jupyter with various languages, and Ruby is one among them. I need to click the Ruby icon to be presented with a screen as shown:

image::jupyter-ec385.png[]

At the left I have file browser where I renamed the file as ruby_injupyter.ipynb, one can get the file here {code_url}/ruby_injupyter.ipynb

So start typing commands like

```ruby
1 + 2
```

Press kbd:[Shiift + Enter] to execute it, and now try out

```ruby
a = 3
b = 5
c = a + b
```

One may also try to use Jupyter for almost this entire book to learn Ruby.
