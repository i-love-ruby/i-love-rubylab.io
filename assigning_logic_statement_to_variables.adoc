=== Assigning logic statement to variables

Wonder whether you noticed or not, in previous example link:code/max_of_nums.rb[max_of_nums.rb] we have used a statement like this

[source, ruby]
----
max = a > b  ? a : b
----

Here `a > b` is logic, if its `true` it would return `a` which gets assigned to `max` or it returns `b` which gets assigned to `max`.

Now the same program can be written as follows

[source, ruby]
----
include::code/max_of_nums_with_if.rb[]
----

Output
----
"max = 5"
----

Here the variable `max` is assigned to an `if` condition. So if `a` is greater than `b` it will put `a` into `max` else it will put `b` in `max`. As simple as that.

Now there is another stuff. What if there are more statements under `if` or `else` ? Since in this code block

[source, ruby]
----
max = if a > b
  a
else
  b
end
----

There is only one statement under `if` block that is `a`, and under `else` block we just have `b`, so it's straight forward. Now let's try out the example given below

[source, ruby]
----
include::code/max_of_nums_with_if_many_statements.rb[]
----

Run the above program and this is what you get

----
"max = 5"
----

So what to infer? The rule is this, if you give many statements in a block and assign it to a variable, the output of the last statement will get returned and will be put into the variable footnote:[You many understand it well when you are reading about <<functions>>] (max in this case).

Here is another program, a fork of link:code/case_when.rb[case_when.rb], I guess you know how it works now

[source, ruby]
----
include::code/case_when_2.rb[]
----

Run it and see it for yourself.
