== Rdoc

You are reading this book because you are looking for some kind of documentation to start programming in Ruby. Documentation is highly important in any kind of programming. Keeping a good documentation for the piece of code you write might distinguish you from a good programmer and make you the one who is sought after. This chapter tells you two things, first where are Ruby's core documentation, and how to find it and read it. The second, it teaches you how to generate documentation so that others can better understand your programs.

=== Reading Ruby Documentation

Let's say that you want to know about `String` class in Ruby. You want to know how to count number of character in ones name using Ruby, so how to do it? Visit this link http://ruby-doc.org/ , it's the centralized place where ruby documentations are available. Now if you can go through it a bit you will find a thing / link like: 3.1.1 core - Core API docs for Ruby 3.1.1. Click on it and you will be directed to this link: http://ruby-doc.org/core-3.1.1/ , its here where core libraries for Ruby 3.1.1 are documented.

A question may arise, how to find out the version of Ruby you are using? In terminal type `ruby -v` it will throw out an output like this: `ruby 3.1.1p18 (2022-02-18 revision 53f5fc4236) [x86_64-darwin21]`, look at the highlighted piece of code, this tells me that I am using ruby 1.9.3p194. Who the heck cares what p194 is? I know I am using ruby 1.9.3, so I am going to see its documentation!

OK, in http://ruby-doc.org/core-3.1.1/ you need to browse for String, if you do you will find this link: http://ruby-doc.org/core-3.1.1/String.html , this is where the `String` class documentation is. You can get to this documentation in this long way or by typing String in the top search bar in which case Rdoc will display valid search results.

The image below shows I am filtering the classes in Ruby by typing Sting into a text box labeled Classes. This will help me to filter the results  / classes easily.

image::rdoc-75999.png[]

OK then, if you have got it right, click on the String to get you here http://ruby-doc.org/core-3.1.1/String.html and browse down, you will find something called `#length` which when clicked will scroll here http://ruby-doc.org/core-3.1.1/String.html#method-i-length, so it does say we have a thing / function / method called length and another function called `size`.

From an educated guess we must be able to know that this is what gives the length of a `String`, let's try it out on irb

[source, ruby]
----
$ irb --simple-prompt
>> "Karthikeyan A K".length
=> 15
>> "Karthikeyan A K".size
=> 15
----

So it works! The basic thing is to reach http://ruby-doc.org/ and break your head, that will get something going and will get you started knowing to read Ruby documentation.

=== Creating Documentation

So hopefully, or hopelessly you might or might not know read ruby's documentation. Lets see how to create one. OK, type the code below in a document called link:code/rdoc_square.rb[rdoc_square.rb], or whatever name you prefer. For simplicity put it into a folder and make sure that no other file is present in that folder.

.{code_url}/rdoc_example/rdoc_square.rb[rdoc_square.rb]
[source, ruby]
----
include::code/rdoc_example/rdoc_square.rb[]
----

Notice how I have added comments footnote:[Its possible to use markdown in Rdoc. To know about markdown visit https://en.wikipedia.org/wiki/Markdown] before attributes and functions. Now navigate to that folder (using console / terminal) where {code_url}/rdoc_example/rdoc_square.rb[rdoc_square.rb] is located and type this command `rdoc`, that's it, the magic happens. You will find a folder named doc created, just navigate into the folder and open file named index.html, you can then click on the Square link in Classes and Modules Index  box to get a nice documentation as shown.

image::rdoc-7e5a8.png[]

In the picture above, in the Attributes section you can see the documentation for side_length attribute,  see just below that is the documentation for it that reads The length of a square's side. Now check the code {code_url}/rdoc_example/rdoc_example.rb[rdoc_example.rb] check the two lines shown below

[source, ruby]
----
class Square

	# The length of a square's side
	attr_accessor :side_length
…....
end
----

We have just added a comment line before `attr_accessor :side_length` that appears on the documentation page. That's how, rdoc determines what to put for documentation. It just checks what are the comments that occurs before the declaration of classes, variables and function definitions and puts it into the documentation, packs it neatly, puts a nice CSS (that is styling) and JavaScript (that's for the dynamic effects footnote:[type something into search box and see]) and gives it to you ready to refer. You can distribute the code as well as the doc folder to other for reference so that people will have better time understanding your code without going through all the lines of ruby coding.

So these are the steps to generating a documentation

* Put commented ruby files into folder
* Navigate to the folder via terminal and type rdoc
* You will see a folder called doc created, just go into the folder and launch the file index.html
